DROP TABLE IF EXISTS credit_line_extension_requests;

CREATE TABLE public.credit_line_extension_requests (
   id IDENTITY NOT NULL,
   extension_amount INTEGER NOT NULL,
   request_date_time TIMESTAMP NOT NULL,
   status VARCHAR(1) NOT NULL,
   approval_date_time TIMESTAMP,
   card_number BIGINT NOT NULL,
   account_number BIGINT NOT NULL,
   channel INTEGER NOT NULL,
   channel_reference BIGINT NOT NULL,
   CONSTRAINT credit_line_extension_requests_pk PRIMARY KEY (id)
);