INSERT INTO credit_line_extension_requests
(extension_amount, request_date_time, status, card_number, channel, channel_reference, account_number)
VALUES
(200, '20210701T15:30:00Z', 'P', 2307830000073373, 2, 155487, 200879);

INSERT INTO credit_line_extension_requests
(extension_amount, request_date_time, status, card_number, channel, channel_reference, account_number)
VALUES
(500, '20210701T16:15:00Z', 'A', 1005478980114879, 2, 998541, 200154);

INSERT INTO credit_line_extension_requests
(extension_amount, request_date_time, status, card_number, channel, channel_reference, account_number)
VALUES
(350, '20210707T12:00:00Z', 'A', 1005478980114879, 1, 225988, 200154);
