package py.com.ci.mcs.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@EnableWebMvc
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
        registry.addResourceHandler("/static/**").addResourceLocations("/public/");
    }

    private final int HTTP_CONNECT_TIMEOUT = 30000;
    private final int HTTP_READ_TIMEOUT = 30000;

    private ClientHttpRequestFactory getClientHttpRequestFactory() {
        HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        clientHttpRequestFactory.setConnectTimeout(HTTP_CONNECT_TIMEOUT);
        clientHttpRequestFactory.setReadTimeout(HTTP_READ_TIMEOUT);
        return clientHttpRequestFactory;
    }

    @Bean
    public RestTemplate getRestClient() {
        RestTemplate  restClient = new RestTemplate(getClientHttpRequestFactory());
        restClient.getMessageConverters().add(new FormHttpMessageConverter());
        restClient.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        return restClient;
    }

}
