package py.com.ci.mcs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import py.com.ci.mcs.model.exception.ExtendAccountLineException;
import py.com.ci.mcs.model.request.ExtendAccountCreditLinePost;
import py.com.ci.mcs.model.request.ExtendAccountCreditLinePostData;
import py.com.ci.mcs.model.response.AccountCreditLineInformationGetData;
import py.com.ci.mcs.model.util.CustomLog;
import py.com.ci.mcs.service.CardService;

@Service
public class CardServiceImpl implements CardService {

    private static final Logger LOGGER = LogManager.getLogger(FinancialDataServiceImpl.class);

    @Autowired
    private RestTemplate restTemplate;

    @Value("${mcs.cards.url}")
    private String url;

    @Override
    public AccountCreditLineInformationGetData findAccountLimit(String logId, Long cardNumber) {
        LOGGER.info(new CustomLog(logId, "Calling to cards service - find account limit", cardNumber));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("log_id", logId);
        HttpEntity entity = new HttpEntity(headers);
        try {
            ResponseEntity response = restTemplate.exchange(
                url + "/account/" + String.valueOf(cardNumber),
                HttpMethod.GET, entity, AccountCreditLineInformationGetData.class
            );
            return (AccountCreditLineInformationGetData) response.getBody();
        }
        catch (HttpClientErrorException ex){
            LOGGER.error(new CustomLog(logId, "Calling error to cards service - find account limit", cardNumber));
            throw new RuntimeException("Calling error to cards service - find account limit: " + ex.getMessage());
        }
    }

    @Override
    public void extendAccountLine(String logId, Long accountNumber, Long extensionAmount) throws ExtendAccountLineException {
        LOGGER.info(new CustomLog(logId, "Calling to cards service - extend account limit", extensionAmount, accountNumber));
        ExtendAccountCreditLinePost extendRequest = new ExtendAccountCreditLinePost();
        extendRequest.setAccountNumber(accountNumber);
        extendRequest.setExtensionAmount(extensionAmount);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("log_id", logId);
        HttpEntity entity = new HttpEntity(new ExtendAccountCreditLinePostData(extendRequest), headers);
        try {
            restTemplate.exchange(
                url + "/account/limit/extend",
                HttpMethod.POST, entity, Object.class
            );
        }
        catch (HttpClientErrorException ex){
            LOGGER.error(new CustomLog(logId, "Calling error to cards service - extend account limit: " + ex.getMessage(), accountNumber, extensionAmount));
            throw new ExtendAccountLineException("Calling error to cards service - extend account limit: " + ex.getMessage());
        }
    }

}
