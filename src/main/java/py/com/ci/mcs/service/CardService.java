package py.com.ci.mcs.service;

import py.com.ci.mcs.model.exception.ExtendAccountLineException;
import py.com.ci.mcs.model.response.AccountCreditLineInformationGetData;

public interface CardService {

    AccountCreditLineInformationGetData findAccountLimit(String logId, Long cardNumber);
    void extendAccountLine(String logId, Long accountNumber, Long extensionAmount) throws ExtendAccountLineException;

}
