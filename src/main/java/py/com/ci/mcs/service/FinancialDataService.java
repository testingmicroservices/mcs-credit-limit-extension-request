package py.com.ci.mcs.service;

import py.com.ci.mcs.model.exception.ExtendAccountLineException;
import py.com.ci.mcs.model.request.CreditLineExtensionRequestPost;
import py.com.ci.mcs.model.response.CreditLineExtensionRequestGet;

import java.util.List;

public interface FinancialDataService {

    List<CreditLineExtensionRequestGet> findByFilters(String logId, Integer channel, String status, Long channelReference, Long accountNumber);
    CreditLineExtensionRequestGet request(String logId, Integer channel, CreditLineExtensionRequestPost data) throws ExtendAccountLineException;
    void approve(String logId, Long requestId) throws ExtendAccountLineException;
    void reject(String logId, Long requestId);

}
