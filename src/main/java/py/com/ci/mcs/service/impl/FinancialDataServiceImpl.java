package py.com.ci.mcs.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import py.com.ci.mcs.converter.CreditLineExtensionRequestConverter;
import py.com.ci.mcs.entity.CreditLineExtensionRequests;
import py.com.ci.mcs.model.Status;
import py.com.ci.mcs.model.exception.ExtendAccountLineException;
import py.com.ci.mcs.model.request.CreditLineExtensionRequestPost;
import py.com.ci.mcs.model.response.AccountCreditLineInformationGet;
import py.com.ci.mcs.model.response.AccountCreditLineInformationGetData;
import py.com.ci.mcs.model.response.CreditLineExtensionRequestGet;
import py.com.ci.mcs.model.util.CustomLog;
import py.com.ci.mcs.repository.FinancialDataRepository;
import py.com.ci.mcs.service.FinancialDataService;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class FinancialDataServiceImpl implements FinancialDataService {

    private static final Logger LOGGER = LogManager.getLogger(FinancialDataServiceImpl.class);

    @Autowired
    private FinancialDataRepository repository;

    @Autowired
    private CreditLineExtensionRequestConverter converter;

    @Autowired
    private CardServiceImpl cardService;

    @Override
    public List<CreditLineExtensionRequestGet> findByFilters(String logId, Integer channel, String status, Long channelReference, Long accountNumber) {
        LOGGER.info(new CustomLog(logId, "findByFilters", channel, status, channelReference, accountNumber).toString());
        return converter.entitiesToModels(repository.findByFilters(status, channel, channelReference, accountNumber));
    }

    @Override
    public CreditLineExtensionRequestGet request(String logId, Integer channel, CreditLineExtensionRequestPost data) throws ExtendAccountLineException {
        LOGGER.info(new CustomLog(logId, "request", channel, data));
        AccountCreditLineInformationGetData accountLimitData = cardService.findAccountLimit(logId, data.getCardNumber());
        AccountCreditLineInformationGet accountLimit = accountLimitData.getData();
        Boolean isApproved = havePreapprovedCreditLineExtension(accountLimit.getPreapprovedAmount(), data.getExtensionAmount());
        if(isApproved){
            LOGGER.info(new CustomLog(logId, "Have preapproved credit line extension", data));
            cardService.extendAccountLine(logId, accountLimit.getAccountNumber(), data.getExtensionAmount());
        }
        else {
            LOGGER.info(new CustomLog(logId, "Not have preapproved credit line extension", data));
        }
        CreditLineExtensionRequests request = converter.modelToEntity(data, channel, accountLimit.getAccountNumber(), isApproved);
        repository.save(request);
        return converter.entityToModel(request);
    }

    @Override
    public void approve(String logId, Long requestId) throws ExtendAccountLineException {
        LOGGER.info(new CustomLog(logId, "approve", requestId));
        Optional<CreditLineExtensionRequests> requestOptional = repository.findById(requestId);
        CreditLineExtensionRequests request = requestOptional.orElseThrow(() -> new RuntimeException("Request not found"));
        cardService.extendAccountLine(logId, request.getAccountNumber(), request.getExtensionAmount());
        request.setStatus(Status.APPROVED.getCode());
        request.setApprovalDateTime(OffsetDateTime.now());
        repository.save(request);
    }

    @Override
    public void reject(String logId, Long requestId) {
        LOGGER.info(new CustomLog(logId, "approve", requestId));
        Optional<CreditLineExtensionRequests> requestOptional = repository.findById(requestId);
        CreditLineExtensionRequests request = requestOptional.orElseThrow(() -> new RuntimeException("Request not found"));
        request.setStatus(Status.REJECTED.getCode());
        request.setApprovalDateTime(OffsetDateTime.now());
        repository.save(request);
    }

    private Boolean havePreapprovedCreditLineExtension(Long preapproved, Long extensionAmount){
        return preapproved.compareTo(extensionAmount) >= 0;
    }

}
