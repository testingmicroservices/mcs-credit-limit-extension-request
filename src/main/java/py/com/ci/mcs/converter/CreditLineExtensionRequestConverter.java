package py.com.ci.mcs.converter;

import org.springframework.stereotype.Component;
import py.com.ci.mcs.entity.CreditLineExtensionRequests;
import py.com.ci.mcs.model.Status;
import py.com.ci.mcs.model.request.CreditLineExtensionRequestPost;
import py.com.ci.mcs.model.response.CreditLineExtensionRequestGet;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
public class CreditLineExtensionRequestConverter {

    public CreditLineExtensionRequestGet entityToModel(CreditLineExtensionRequests entity){
        CreditLineExtensionRequestGet model = new CreditLineExtensionRequestGet();
        model.setChannel(entity.getChannel());
        model.setApprovalDateTime(entity.getApprovalDateTime());
        model.setCardNumber(entity.getCardNumber());
        model.setChannelReference(entity.getChannelReference());
        model.setStatus(entity.getStatus());
        model.setRequestDateTime(entity.getRequestDateTime());
        model.setId(entity.getId());
        model.setExtensionAmount(entity.getExtensionAmount());
        model.setAccountNumber(entity.getAccountNumber());
        return model;
    }

    public List<CreditLineExtensionRequestGet> entitiesToModels(List<CreditLineExtensionRequests> entities){
        List<CreditLineExtensionRequestGet> models = new ArrayList<>();
        if(entities == null || entities.isEmpty()){
            return models;
        }
        for(CreditLineExtensionRequests entity : entities){
            models.add(entityToModel(entity));
        }
        return models;
    }

    public CreditLineExtensionRequests modelToEntity(CreditLineExtensionRequestPost model, Integer channel, Long accountNumber, Boolean approved){
        CreditLineExtensionRequests entity = new CreditLineExtensionRequests();
        entity.setCardNumber(model.getCardNumber());
        entity.setChannel(channel);
        entity.setChannelReference(model.getChannelReference());
        entity.setRequestDateTime(OffsetDateTime.now());
        entity.setStatus(Status.PENDIENT.getCode());
        if(approved){
            entity.setApprovalDateTime(OffsetDateTime.now());
            entity.setStatus(Status.APPROVED.getCode());
        }
        entity.setExtensionAmount(model.getExtensionAmount());
        entity.setAccountNumber(accountNumber);
        return entity;
    }

}
