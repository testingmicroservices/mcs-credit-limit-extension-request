package py.com.ci.mcs.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "credit_line_extension_requests")
public class CreditLineExtensionRequests {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "extension_amount")
    private Long extensionAmount;

    @Column(name = "request_date_time")
    private OffsetDateTime requestDateTime;

    @Column(name = "status")
    private String status;

    @Column(name = "approval_date_time")
    private OffsetDateTime approvalDateTime;

    @Column(name = "card_number")
    private Long cardNumber;

    @Column(name = "account_number")
    private Long accountNumber;

    @Column(name = "channel")
    private Integer channel;

    @Column(name = "channel_reference")
    private Long channelReference;

}
