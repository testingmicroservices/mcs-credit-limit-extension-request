package py.com.ci.mcs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import py.com.ci.mcs.model.request.CreditLineExtensionRequestPostData;
import py.com.ci.mcs.model.response.CreditLineExtensionRequestGet;
import py.com.ci.mcs.model.response.CreditLineExtensionRequestGetData;
import py.com.ci.mcs.model.response.CreditLineExtensionRequestGetListData;
import py.com.ci.mcs.service.FinancialDataService;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/application")
public class FinancialDataController {

    @Autowired
    private FinancialDataService financialDataService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity request(
        @RequestHeader(value = "channel", required = true) Integer channel,
        @RequestHeader(value = "log_id", required = true) String logId,
        @RequestBody CreditLineExtensionRequestPostData data
    ){
        try {
            CreditLineExtensionRequestGet saved = financialDataService.request(logId, channel, data.getData());
            return new ResponseEntity(new CreditLineExtensionRequestGetData(saved), HttpStatus.CREATED);
        }
        catch (Throwable ex){
            return new ResponseEntity(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/{request_id}/approve")
    public ResponseEntity approve(
        @RequestHeader(value = "log_id", required = true) String logId,
        @PathVariable("request_id") Long requestId
    ){
        try {
            financialDataService.approve(logId, requestId);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (Throwable ex){
            return new ResponseEntity(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/{request_id}/reject")
    public ResponseEntity reject(
            @RequestHeader(value = "log_id", required = true) String logId,
            @PathVariable("request_id") Long requestId
    ){
        try {
            financialDataService.reject(logId, requestId);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (Throwable ex){
            return new ResponseEntity(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping
    public ResponseEntity findByFilters(
        @RequestHeader(value = "channel", required = false) Integer channel,
        @RequestHeader(value = "log_id", required = true) String logId,
        @RequestParam(value = "channel_reference", required = false) Long channelReference,
        @RequestParam(value = "status", required = false) String status,
        @RequestParam(value = "account_number", required = false) Long accountNumber
    ){
        try {
            List<CreditLineExtensionRequestGet> data = financialDataService
            .findByFilters(logId, channel, status, channelReference, accountNumber);
            if(data == null || data.isEmpty()){
                return new ResponseEntity(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity(new CreditLineExtensionRequestGetListData(data), HttpStatus.OK);
        }
        catch (Throwable ex){
            return new ResponseEntity(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
