package py.com.ci.mcs.model.util;

public class CustomLog {

    private String logId;
    private String message;
    private Object[] params;

    public CustomLog(String logId, String message, Object... params){
        this.logId = logId;
        this.message = message;
        this.params = params;
    }

    public CustomLog(String logId, String message){
        this.logId = logId;
        this.message = message;
    }

    public CustomLog(String message){
        this.message = message;
    }

    public CustomLog(String message, Object... params){
        this.message = message;
        this.params = params;
    }

    @Override
    public String toString() {
        return
            (logId == null ? "NOT LOG_ID PRESENT" : (logId + " | ")) +
            message +
            (params == null || params.length == 0 ? "" : " | " + printParams());
    }

    private String printParams(){
        String[] paramsString = new String[params.length];
        for(int i = 0; i < params.length; i++){
            paramsString[i] = new String(params[i] == null ? "" : params[i].toString());
        }
        return String.join(",", paramsString);
    }
}
