package py.com.ci.mcs.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreditLineExtensionRequestPost implements Serializable {

    @JsonProperty("card_number")
    private Long cardNumber;

    @JsonProperty("extension_amount")
    private Long extensionAmount;

    @JsonProperty("channel_reference")
    private Long channelReference;

}
