package py.com.ci.mcs.model.exception;

public class ExtendAccountLineException extends Exception{

    public ExtendAccountLineException(String message){
        super(message);
    }

}
