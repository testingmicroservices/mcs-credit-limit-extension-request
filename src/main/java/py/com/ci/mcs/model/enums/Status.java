package py.com.ci.mcs.model.enums;

public enum Status {

    PENDIENT("P"),
    APPROVED("A"),
    REJECTED("R");

    private String code;

    Status(String code){
        this.code = code;
    }

    public String getCode(){
        return this.code;
    }

}
