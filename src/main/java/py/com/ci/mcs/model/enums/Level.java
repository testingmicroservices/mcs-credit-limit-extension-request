package py.com.ci.mcs.model.enums;

public enum Level {

    ERROR,
    INFO,
    WARNING;

}
