package py.com.ci.mcs.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreditLineExtensionRequestGetData implements Serializable {

    @JsonProperty("data")
    private CreditLineExtensionRequestGet data;

}
