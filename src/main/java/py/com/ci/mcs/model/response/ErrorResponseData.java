package py.com.ci.mcs.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import py.com.ci.mcs.model.enums.Level;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorResponseData implements Serializable {

    private ErrorResponse data;

    public ErrorResponseData(String message, HttpStatus status, Level level){
        this.data = new ErrorResponse();
        this.data.setMessage(message);
        this.data.setStatus(status.value());
        this.data.setLevel(level.toString());
    }


}
