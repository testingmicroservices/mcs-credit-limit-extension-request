package py.com.ci.mcs.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountCreditLineInformationGet {

    @JsonProperty("credit_line")
    private Long creditLine;

    @JsonProperty("preapproved_amount")
    private Long preapprovedAmount;

    @JsonProperty("first_name")
    private String firstName;

    @JsonProperty("last_name")
    private String lastName;

    @JsonProperty("account_number")
    private Long accountNumber;

    @JsonProperty("card_number")
    private Long cardNumber;

}
