package py.com.ci.mcs.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import py.com.ci.mcs.model.util.OffsetDateTimeDeserializer;
import py.com.ci.mcs.model.util.OffsetDateTimeSerializer;

import java.io.Serializable;
import java.time.OffsetDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreditLineExtensionRequestGet implements Serializable {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("card_number")
    private Long cardNumber;

    @JsonProperty("account_number")
    private Long accountNumber;

    @JsonProperty("extension_amount")
    private Long extensionAmount;

    @JsonProperty("status")
    private String status;

    @JsonSerialize(using = OffsetDateTimeSerializer.class)
    @JsonDeserialize(using = OffsetDateTimeDeserializer.class)
    @JsonProperty("request_date_time")
    private OffsetDateTime requestDateTime;

    @JsonProperty("channel")
    private Integer channel;

    @JsonProperty("channel_reference")
    private Long channelReference;

    @JsonSerialize(using = OffsetDateTimeSerializer.class)
    @JsonDeserialize(using = OffsetDateTimeDeserializer.class)
    @JsonProperty("approval_date_time")
    private OffsetDateTime approvalDateTime;

}
