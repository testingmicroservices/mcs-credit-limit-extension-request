package py.com.ci.mcs.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import py.com.ci.mcs.entity.CreditLineExtensionRequests;

import java.util.List;

@Repository
public interface FinancialDataRepository extends CrudRepository<CreditLineExtensionRequests, Long> {

    @Query(
        "SELECT C FROM CreditLineExtensionRequests C " +
        "WHERE " +
            "(:status IS NULL OR C.status = :status) AND " +
            "(:channel IS NULL OR C.channel = :channel) AND " +
            "(:channel_reference IS NULL OR C.channelReference = :channel_reference) AND " +
            "(:account_number IS NULL OR C.accountNumber = :account_number)"
    )
    List<CreditLineExtensionRequests> findByFilters(
        @Param("status") String status, @Param("channel") Integer channel,
        @Param("channel_reference") Long channelReference,
        @Param("account_number") Long accountNumber
    );

}
