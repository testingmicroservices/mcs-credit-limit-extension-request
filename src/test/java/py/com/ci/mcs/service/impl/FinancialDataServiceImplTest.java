package py.com.ci.mcs.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import py.com.ci.mcs.converter.CreditLineExtensionRequestConverter;
import py.com.ci.mcs.entity.CreditLineExtensionRequests;
import py.com.ci.mcs.model.Status;
import py.com.ci.mcs.model.request.CreditLineExtensionRequestPost;
import py.com.ci.mcs.model.response.AccountCreditLineInformationGet;
import py.com.ci.mcs.model.response.AccountCreditLineInformationGetData;
import py.com.ci.mcs.model.response.CreditLineExtensionRequestGet;
import py.com.ci.mcs.repository.FinancialDataRepository;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

@SpringBootTest
public class FinancialDataServiceImplTest {

    @InjectMocks
    private FinancialDataServiceImpl creditLineExtensionRequestService;

    @Mock
    private FinancialDataRepository repository;

    @Mock
    private CreditLineExtensionRequestConverter converter;

    @Mock
    private CardServiceImpl cardService;

    @BeforeEach
    public void setup(){
        Mockito.doCallRealMethod().when(converter).entityToModel(any());
        Mockito.doCallRealMethod().when(converter).entitiesToModels(any());
        Mockito.doCallRealMethod().when(converter).modelToEntity(any(), any(), any(), any());
    }

    @Test
    public void findByFilters() {
        System.out.println("findByFilters | CreditLineExtensionRequestServiceImplTest");

        String logId = UUID.randomUUID().toString();
        Integer channel = 1;
        String status = "P";
        Long channelReference = 1L;
        Long accountNumber = 1L;

        Mockito
        .doReturn(mockRequests())
        .when(repository)
        .findByFilters(status, channel, channelReference, accountNumber);

        List<CreditLineExtensionRequestGet> requests = creditLineExtensionRequestService.findByFilters(
            logId, channel, status, channelReference, accountNumber
        );

        requests.forEach((req) -> {
            assertTrue(status.equals(req.getStatus()));
            assertTrue(accountNumber.equals(req.getAccountNumber()));
            assertTrue(channel.equals(req.getChannel()));
        });
    }

    @Test
    public void testRequestWithPreapprovedAmount() throws Exception {
        System.out.println("request, with preapproved amount | CreditLineExtensionRequestServiceImplTest");

        String logId = UUID.randomUUID().toString();
        Integer channel = 1;

        CreditLineExtensionRequestPost data = new CreditLineExtensionRequestPost();
        data.setCardNumber(1005478980114879L);
        data.setExtensionAmount(200L);
        data.setChannelReference(1L);

        AccountCreditLineInformationGetData accountInformation = mockAccountInformation(200L);

        Mockito
        .doReturn(accountInformation)
        .when(cardService)
        .findAccountLimit(logId, 1005478980114879L);

        Mockito
        .doNothing()
        .when(cardService)
        .extendAccountLine(logId, accountInformation.getData().getAccountNumber(), 200L);

        CreditLineExtensionRequestGet request = creditLineExtensionRequestService.request(logId, channel, data);

        assertEquals(Status.APPROVED.getCode(), request.getStatus());
        assertTrue(accountInformation.getData().getAccountNumber().equals(request.getAccountNumber()));

    }

    @Test
    public void testRequestWithoutPreapprovedAmount() throws Exception {
        System.out.println("request, without preapproved amount | CreditLineExtensionRequestServiceImplTest");

        String logId = UUID.randomUUID().toString();
        Integer channel = 1;

        CreditLineExtensionRequestPost data = new CreditLineExtensionRequestPost();
        data.setCardNumber(1005478980114879L);
        data.setExtensionAmount(200L);
        data.setChannelReference(1L);

        AccountCreditLineInformationGetData accountInformation = mockAccountInformation(100L);

        Mockito
        .doReturn(accountInformation)
        .when(cardService)
        .findAccountLimit(logId, 1005478980114879L);

        CreditLineExtensionRequestGet request = creditLineExtensionRequestService.request(logId, channel, data);

        assertEquals(Status.PENDIENT.getCode(), request.getStatus());
        assertTrue(accountInformation.getData().getAccountNumber().equals(request.getAccountNumber()));
    }

    @Test
    public void approve() throws Exception {
        System.out.println("approve | CreditLineExtensionRequestServiceImplTest");

        String logId = UUID.randomUUID().toString();
        Long requestId = 1L;

        Optional<CreditLineExtensionRequests> requestOp = Optional.of(mockRequest());

        Mockito
        .doReturn(requestOp)
        .when(repository)
        .findById(requestId);

        Mockito
        .doNothing()
        .when(cardService)
        .extendAccountLine(logId, 1L, 200L);

        creditLineExtensionRequestService.approve(logId, requestId);
        assertEquals(Status.APPROVED.getCode(), requestOp.get().getStatus());
    }

    @Test
    public void reject() {
        System.out.println("approve | CreditLineExtensionRequestServiceImplTest");

        String logId = UUID.randomUUID().toString();
        Long requestId = 1L;

        Optional<CreditLineExtensionRequests> requestOp = Optional.of(mockRequest());

        Mockito
        .doReturn(requestOp)
        .when(repository)
        .findById(requestId);

        creditLineExtensionRequestService.reject(logId, requestId);
        assertEquals(Status.REJECTED.getCode(), requestOp.get().getStatus());
    }

    private List<CreditLineExtensionRequests> mockRequests(){
        List<CreditLineExtensionRequests> requests = new ArrayList<>();
        requests.add(mockRequest());
        return requests;
    }

    private CreditLineExtensionRequests mockRequest(){
        CreditLineExtensionRequests request = new CreditLineExtensionRequests();
        request.setStatus("P");
        request.setAccountNumber(1L);
        request.setExtensionAmount(200L);
        request.setChannel(1);
        request.setChannelReference(1L);
        request.setRequestDateTime(OffsetDateTime.now());
        request.setCardNumber(1005478980114879L);
        request.setId(1L);
        return request;
    }

    private AccountCreditLineInformationGetData mockAccountInformation(Long extensionAmount){
        AccountCreditLineInformationGet data = new AccountCreditLineInformationGet();
        data.setAccountNumber(1L);
        data.setCardNumber(1005478980114879L);
        data.setCreditLine(extensionAmount*2);
        data.setPreapprovedAmount(extensionAmount);
        data.setFirstName("Roberto");
        data.setLastName("Paredes");
        return new AccountCreditLineInformationGetData(data);
    }

}