package py.com.ci.mcs;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureStubRunner(
    ids = {"py.com.ci:mcs.cards:+:stubs:6565"},
    stubsMode = StubRunnerProperties.StubsMode.LOCAL
)
@AutoConfigureMockMvc
@AutoConfigureJsonTesters
@DirtiesContext
public class CreditLineExtensionRequestTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void givenValidBodyInExtensionRequestThenReturnCreated() throws Exception {
        mockMvc.perform(
        MockMvcRequestBuilders.post("/application")
            .contentType(MediaType.APPLICATION_JSON)
            .header("log_id", "1")
            .header("channel", 1L)
            .content("{\n" +
                    "  \"data\": {\n" +
                    "    \"extension_amount\": 200,\n" +
                    "    \"card_number\": 1005478980114879,\n" +
                    "    \"channel_reference\": 1\n" +
                    "  }\n" +
            "}")
        )
        .andExpect(status().isCreated());
    }

    @Test
    public void givenInexistentCardNumberInExtensionRequestThenReturnInternalServerError() throws Exception {
        mockMvc.perform(
        MockMvcRequestBuilders.post("/application")
            .contentType(MediaType.APPLICATION_JSON)
            .header("log_id", "1")
            .header("channel", 1L)
            .content("{\n" +
                    "  \"data\": {\n" +
                    "    \"extension_amount\": 200,\n" +
                    "    \"card_number\": 1,\n" +
                    "    \"channel_reference\": 1\n" +
                    "  }\n" +
            "}")
        )
        .andExpect(status().isInternalServerError());
    }

}