# MCS Financial Data
### Descarga del repositorio
1. Abrir la consola Git Bash
2. Dirigirse al directorio en donde se desea descargar el proyecto, comando <strong>`cd`</strong>
3. Ingrese el comando <strong>`git clone https://gitlab.com/testingmicroservices/mcs-credit-limit-extension-request.git`</strong>
4. Acceda a la carpeta descargada con el comando <strong>`cd mcs.financial.data`</strong>

### Abrir el proyecto con Intellij Community Edition
1. Inicie el programa Intellij Community Edition
2. Acceda a `Archivo > Abrir > mcs.financial.data`
3. Acceda al menú de maven desde el menu lateral derecho
<br></br>
    ![Menu lateral derecho](/assets/menu-derecho.png "Menu lateral derecho")
<br></br>
4. Despliegue el menú del proyecto mcs.financial.data y haga doble click en install
<br></br>
    ![Menu desplegado](/assets/menu-desplegado-install.png "Menu desplegado")
    <br></br>
5. Compilación exitosa
<br></br>
    ![Compilación exitosa](/assets/mvn-install.png "Compilación exitosa")